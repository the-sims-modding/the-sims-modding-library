import platform
import re
from itertools import count
from pathlib import Path
from typing import Iterator, List, Tuple, Union

_game_install_directory = None


def get_game_install_directory() -> Path:
    global _game_install_directory

    if _game_install_directory:
        return _game_install_directory

    system = platform.system()
    if system == "Windows":
        from winreg import HKEY_LOCAL_MACHINE, OpenKey, QueryValueEx

        with OpenKey(HKEY_LOCAL_MACHINE, r"SOFTWARE\Maxis\The Sims 4") as key:
            install_dir, _ = QueryValueEx(key, "Install Dir")
            _game_install_directory = Path(install_dir)
        return _game_install_directory
    else:
        raise Exception("Only supported on Windows")


_pack_regex = re.compile(r"\wP\d\d")


def get_packs(*, only_installed: bool = True) -> List[str]:
    install_dir = get_game_install_directory()
    packs = ["BASE_GAME"]
    for path in install_dir.iterdir():
        if _pack_regex.fullmatch(path.name):
            packs.append(path.name.upper())
    if only_installed:
        return packs
    for path in (install_dir / "Delta").iterdir():
        if _pack_regex.fullmatch(path.name) and path.name not in packs:
            packs.append(path.name.upper())
    return packs


def find_strings_packages(
    *, only_installed_packs: bool = True
) -> Iterator[Tuple[str, Path]]:
    install_dir = get_game_install_directory()
    for pack in get_packs(only_installed=only_installed_packs):
        for delta_path in install_dir.glob(
            "Data/Client/Strings_*.package"
            if pack == "BASE_GAME"
            else f"Delta/{pack}/Strings_*.package"
        ):
            yield pack, delta_path
            break
        else:
            for full_path in install_dir.glob(f"{pack}/Strings_*.package"):
                yield pack, full_path
                break


def find_build_packages(
    *, only_installed_packs: bool = True, simulation: bool = False
) -> Iterator[Tuple[str, Union[Path, Tuple[Path, Path]]]]:
    install_dir = get_game_install_directory()
    kind = "Simulation" if simulation else "Client"
    for pack in get_packs(only_installed=only_installed_packs):
        if pack != "BASE_GAME":
            full_path = install_dir / pack / f"{kind}FullBuild0.package"
            delta_path = install_dir / "Delta" / pack / f"{kind}DeltaBuild0.package"
            if full_path.exists():
                if delta_path.exists():
                    yield pack, (full_path, delta_path)
                else:
                    yield pack, full_path
            elif delta_path.exists():
                yield pack, delta_path
            continue
        for i in count():
            full_path = install_dir / f"Data/{kind}/{kind}FullBuild{i}.package"
            delta_path = install_dir / f"Data/{kind}/{kind}DeltaBuild{i}.package"
            if full_path.exists():
                if delta_path.exists():
                    yield pack, (full_path, delta_path)
                else:
                    yield pack, full_path
            elif delta_path.exists():
                yield pack, delta_path
            else:
                break
