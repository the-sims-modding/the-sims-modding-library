import re
from dataclasses import dataclass
from enum import Enum
from itertools import groupby
from typing import Iterable, Iterator, List, Optional, Union


class Element:
    pass


@dataclass
class Tag(Element):
    contents: str

    def __str__(self):
        return f"<{self.contents}>"


@dataclass
class Template(Element):
    contents: str

    def __str__(self):
        return f"{{{self.contents}}}"


class Gender(Enum):
    FEMALE = "F"
    MALE = "M"
    UNISEX = "U"


@dataclass
class GenderTemplate(Element):
    gender: Gender
    index: int
    phrase: str

    def __str__(self):
        return f"{{{self.gender.value}{self.index}.{self.phrase}}}"


_gender_template_regex = re.compile(r"{(F|M|U)(\d+)\.(.*)}", re.IGNORECASE)


def _parse_element(s: str) -> Element:
    if s[0] == "<":
        return Tag(s[1:-1])
    match = _gender_template_regex.fullmatch(s)
    if match:
        g, i, p = match.groups()
        return GenderTemplate(Gender(g.upper()), int(i), p)
    return Template(s[1:-1])


Part = Union[str, Element]
_extraction_regex = re.compile(r"([^<{]*)(<[^>]*>|{[^}]*})(.*)")


def dissect_string(string: str) -> Iterator[Part]:
    while string:
        match = _extraction_regex.fullmatch(string)
        if not match:
            yield string
            return
        pre, element, post = match.groups()
        if pre:
            yield pre
        yield _parse_element(element)
        string = post


@dataclass
class MergedGenderTemplates(Element):
    index: int
    female: str
    male: str
    unisex: Optional[str]

    def __str__(self):
        female = GenderTemplate(Gender.FEMALE, self.index, self.female)
        male = GenderTemplate(Gender.MALE, self.index, self.male)
        if not self.unisex:
            return f"{female}{male}"
        unisex = GenderTemplate(Gender.UNISEX, self.index, self.unisex)
        return f"{female}{male}{unisex}"


class TemplateError(Exception):
    pass


_required_genders = {Gender.FEMALE, Gender.MALE}


def merge_gender_templates(parts: Iterable[Part]) -> Iterator[Part]:
    for match, sub_parts in groupby(parts, lambda p: isinstance(p, GenderTemplate)):
        if not match:
            yield from sub_parts
            continue
        gender_parts: List[GenderTemplate] = list(sub_parts)  # type: ignore
        genders = set(p.gender for p in gender_parts)
        if len(genders) != len(gender_parts):
            raise TemplateError("duplicate gender")
        if not genders >= _required_genders:
            raise TemplateError("required gender missing")
        if len(set(p.index for p in gender_parts)) > 1:
            raise TemplateError("mismatched index")
        gender_parts.sort(key=lambda p: p.gender.value)
        yield MergedGenderTemplates(
            gender_parts[0].index,
            gender_parts[0].phrase,
            gender_parts[1].phrase,
            gender_parts[2].phrase if len(gender_parts) > 2 else None,
        )


def assemble_string(parts: Iterable[Part]) -> str:
    return "".join(map(str, parts))
