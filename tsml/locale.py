from enum import IntEnum


class Locale(IntEnum):
    ENG_US = 0x00
    ENGLISH = ENG_US


def get_locale(instance: int) -> Locale:
    return Locale(instance >> 56)


_inv_locale_mask = (1 << 56) - 1


def set_locale(instance: int, locale: Locale) -> int:
    return (instance & _inv_locale_mask) + (locale << 56)
