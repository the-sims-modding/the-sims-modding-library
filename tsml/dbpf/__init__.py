from dataclasses import dataclass
from itertools import chain
from os import PathLike
from struct import Struct
from typing import (
    IO,
    Callable,
    ContextManager,
    Dict,
    Iterator,
    Mapping,
    MutableMapping,
    NamedTuple,
    SupportsBytes,
    Tuple,
    Union,
)

from ..resource_key import ResourceKey
from .compression import Compression, compress, decompress
from .index import Index, IndexEntry


class UnsupportedPackage(Exception):
    pass


class CorruptPackage(Exception):
    pass


class Version(NamedTuple):
    major: int
    minor: int


_header_struct = Struct("<4s 2I 2I 4x 2I 4x 3I 12x I Q 24x")
_header_identifier = b"DBPF"
_header_format_version = Version(2, 1)


@dataclass
class _Header:
    file_version: Version = Version(0, 0)
    creation_time: int = 0
    update_time: int = 0
    index_count: int = 0
    index_offset: int = 0
    index_size: int = 0

    def read(self, stream: IO[bytes]) -> None:
        (
            identifier,
            format_version_major,
            format_version_minor,
            file_version_major,
            file_version_minor,
            self.creation_time,
            self.update_time,
            self.index_count,
            index_offset_small,
            self.index_size,
            constant_3,
            index_offset_large,
        ) = _header_struct.unpack(stream.read(_header_struct.size))
        if identifier != _header_identifier or constant_3 != 3:
            raise CorruptPackage
        if (format_version_major, format_version_minor) != _header_format_version:
            raise UnsupportedPackage
        self.file_version = Version(file_version_major, file_version_minor)
        self.index_offset = index_offset_small or index_offset_large

    def write(self, stream: IO[bytes]) -> None:
        stream.write(
            _header_struct.pack(
                _header_identifier,
                _header_format_version.major,
                _header_format_version.minor,
                self.file_version.major,
                self.file_version.minor,
                self.creation_time,
                self.update_time,
                self.index_count,
                0,
                self.index_size,
                3,
                self.index_offset,
            )
        )


class _Resource:
    def __init__(
        self,
        *,
        compressed: bytes,
        compression: int,
        uncompressed: bytes = None,
        uncompressed_size: int,
    ) -> None:
        self._compressed = compressed
        self._compressed_size = len(compressed)
        self._compression = compression
        self._uncompressed = uncompressed
        self._uncompressed_size = uncompressed_size

    def __bytes__(self) -> bytes:
        if not self._uncompressed:
            self._uncompressed = decompress(self._compressed, self._compression)
        return self._uncompressed


class Package(
    ContextManager["Package"], MutableMapping[ResourceKey, Union[bytes, SupportsBytes]]
):
    def __init__(self, path: PathLike, mode: str = "r") -> None:
        if mode not in ("r", "w"):
            raise ValueError(f"invalid mode: {mode!r}")
        self._mode = mode
        self._file = open(path, self._mode + "b")
        if self._mode in ("r",):
            header = _Header()
            header.read(self._file)
            self._file.seek(header.index_offset)
            index = Index()
            index.read(self._file, header.index_count)
            self._existing_entries = {
                k: e
                for k, e in index.entries.items()
                if e.compression != Compression.DELETED
            }
            self._deleted_keys = {
                k
                for k, e in index.entries.items()
                if e.compression == Compression.DELETED
            }
        else:
            self._existing_entries = {}
            self._deleted_keys = set()
        self._new_resources: Dict[ResourceKey, _Resource] = {}

    def resources(
        self, key_pred: Callable[[ResourceKey], bool] = None
    ) -> Iterator[Tuple[ResourceKey, SupportsBytes]]:
        return ((k, self[k]) for k in self if not key_pred or key_pred(k))

    def close(self) -> None:
        self._file.close()

    def commit(self) -> None:
        self._file.seek(_header_struct.size)
        entries = {}
        for k, r in self._new_resources.items():
            entries[k] = IndexEntry(
                offset=self._file.tell(),
                compressed_size=r._compressed_size,
                uncompressed_size=r._uncompressed_size,
                compression=r._compression,
            )
            self._file.write(r._compressed)
        index_offset = self._file.tell()
        index = Index(entries)
        index.write(self._file)
        index_size = self._file.tell() - index_offset
        self._file.seek(0)
        header = _Header(
            index_count=len(entries), index_offset=index_offset, index_size=index_size,
        )
        header.write(self._file)

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        if self._mode in ("w",) and not (exc_type or exc_value or traceback):
            self.commit()
        self.close()

    def __getitem__(self, key: ResourceKey) -> SupportsBytes:
        if key in self._new_resources:
            return self._new_resources[key]
        entry = self._existing_entries[key]
        self._file.seek(entry.offset)
        data = self._file.read(entry.compressed_size)
        return _Resource(
            compressed=data,
            compression=entry.compression,
            uncompressed_size=entry.uncompressed_size,
        )

    def __setitem__(self, key: ResourceKey, value: Union[bytes, SupportsBytes]) -> None:
        if key in self._existing_entries:
            del self._existing_entries[key]
        if key in self._deleted_keys:
            self._deleted_keys.remove(key)
        if isinstance(value, _Resource):
            self._new_resources[key] = value
            return
        data = bytes(value)
        self._new_resources[key] = _Resource(
            compressed=compress(data, Compression.ZLIB),
            compression=Compression.ZLIB,
            uncompressed=data,
            uncompressed_size=len(data),
        )

    def __delitem__(self, key: ResourceKey) -> None:
        if key in self._new_resources:
            del self._new_resources[key]
        else:
            del self._existing_entries[key]

    def __iter__(self) -> Iterator[ResourceKey]:
        return chain(self._existing_entries, self._new_resources)

    def __len__(self) -> int:
        return len(self._existing_entries) + len(self._new_resources)


class PatchedPackage(
    ContextManager["PatchedPackage"], Mapping[ResourceKey, SupportsBytes]
):
    def __init__(self, full_path: PathLike, delta_path: PathLike) -> None:
        self._full = Package(full_path)
        self._delta = Package(delta_path)
        self._keys = set(self._delta) | (set(self._full) - self._delta._deleted_keys)

    def resources(
        self, key_pred: Callable[[ResourceKey], bool] = None
    ) -> Iterator[Tuple[ResourceKey, SupportsBytes]]:
        return ((k, self[k]) for k in self if not key_pred or key_pred(k))

    def close(self) -> None:
        self._full.close()
        self._delta.close()

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.close()

    def __getitem__(self, key: ResourceKey) -> SupportsBytes:
        if key in self._delta:
            return self._delta[key]
        if key in self._delta._deleted_keys:
            raise KeyError(key)
        return self._full[key]

    def __iter__(self) -> Iterator[ResourceKey]:
        return iter(self._keys)

    def __len__(self) -> int:
        return len(self._keys)


def open_package(
    paths: Union[PathLike, Tuple[PathLike, PathLike]]
) -> Union[Package, PatchedPackage]:
    if isinstance(paths, tuple):
        return PatchedPackage(*paths)
    return Package(paths)
