from dataclasses import dataclass, field
from struct import Struct
from typing import IO, Dict

from ..resource_key import ResourceKey
from .compression import Compression


class UnsupportedPackageIndex(Exception):
    pass


@dataclass
class IndexEntry:
    offset: int = 0
    compressed_size: int = 0
    uncompressed_size: int = 0
    compression: int = Compression.ZLIB
    committed: int = 1


_int32_struct = Struct("<I")

_full_entry_struct = Struct("<4I 3I 2H")


@dataclass
class Index:
    entries: Dict[ResourceKey, IndexEntry] = field(default_factory=dict)

    def read(self, stream: IO[bytes], count: int) -> None:
        (flags,) = _int32_struct.unpack(stream.read(4))
        entries: Dict[ResourceKey, IndexEntry] = {}
        if not flags:
            entry_struct = _full_entry_struct
            for _ in range(count):
                (
                    resource_type,
                    resource_group,
                    resource_instance_high,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x1:
            (resource_type,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<3I 3I 2H")
            for _ in range(count):
                (
                    resource_group,
                    resource_instance_high,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x2:
            (resource_group,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<3I 3I 2H")
            for _ in range(count):
                (
                    resource_type,
                    resource_instance_high,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x3:
            (resource_type,) = _int32_struct.unpack(stream.read(4))
            (resource_group,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<2I 3I 2H")
            for _ in range(count):
                (
                    resource_instance_high,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x4:
            (resource_instance_high,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<3I 3I 2H")
            for _ in range(count):
                (
                    resource_type,
                    resource_group,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x5:
            (resource_type,) = _int32_struct.unpack(stream.read(4))
            (resource_instance_high,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<2I 3I 2H")
            for _ in range(count):
                (
                    resource_group,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x6:
            (resource_group,) = _int32_struct.unpack(stream.read(4))
            (resource_instance_high,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<2I 3I 2H")
            for _ in range(count):
                (
                    resource_type,
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        elif flags == 0x7:
            (resource_type,) = _int32_struct.unpack(stream.read(4))
            (resource_group,) = _int32_struct.unpack(stream.read(4))
            (resource_instance_high,) = _int32_struct.unpack(stream.read(4))
            entry_struct = Struct("<I 3I 2H")
            for _ in range(count):
                (
                    resource_instance_low,
                    offset,
                    compressed_size,
                    uncompressed_size,
                    compression,
                    committed,
                ) = entry_struct.unpack(stream.read(entry_struct.size))
                if not compressed_size & 0x80000000:
                    raise UnsupportedPackageIndex
                entries[
                    ResourceKey(
                        resource_type,
                        resource_group,
                        (resource_instance_high << 32) + resource_instance_low,
                    )
                ] = IndexEntry(
                    offset,
                    compressed_size & 0x7FFFFFFF,
                    uncompressed_size,
                    compression,
                    committed,
                )
        else:
            raise UnsupportedPackageIndex
        self.entries = entries

    def write(self, stream: IO[bytes]) -> None:
        stream.write(_int32_struct.pack(0))
        for key, entry in self.entries.items():
            stream.write(
                _full_entry_struct.pack(
                    key.type,
                    key.group,
                    key.instance >> 32,
                    key.instance & 0xFFFFFFFF,
                    entry.offset,
                    entry.compressed_size | 0x80000000,
                    entry.uncompressed_size,
                    entry.compression,
                    entry.committed,
                )
            )
