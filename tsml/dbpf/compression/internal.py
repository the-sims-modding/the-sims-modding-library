def decompress(source: bytes) -> bytes:
    # Read the header.
    # size: The size of the uncompressed data.
    # sp: The current source position.
    size = (source[2] << 16) + (source[3] << 8) + source[4]
    sp = 5
    if source[0] & 0x80:
        size = (size << 8) + source[6]
        sp += 1
    # Define additional variables.
    # dest: Array for storing the decompressed data.
    # dp: The current dest position.
    # last_command: Whether the last decompression command is issued.
    dest = bytearray(size)
    dp = 0
    last_command = False
    # Decompress the data.
    while not last_command:
        # Read the next decompression command.
        # sn: Number of bytes to copy from source.
        # dn: Number of bytes to copy from dest.
        # do: Offset for copying from dest.
        b0, sp = source[sp], sp + 1
        if b0 < 0x80:
            b1, sp = source[sp], sp + 1
            sn = b0 & 0x03
            dn = ((b0 & 0x1C) >> 2) + 3
            do = ((b0 & 0x60) << 3) + b1 + 1
        elif b0 < 0xC0:
            b1, b2, sp = source[sp], source[sp + 1], sp + 2
            sn = (b1 & 0xC0) >> 6
            dn = (b0 & 0x3F) + 4
            do = ((b1 & 0x3F) << 8) + b2 + 1
        elif b0 < 0xE0:
            b1, b2, b3, sp = source[sp], source[sp + 1], source[sp + 2], sp + 3
            sn = b0 & 0x03
            dn = ((b0 & 0x0C) << 6) + b3 + 5
            do = ((b0 & 0x10) << 12) + (b1 << 8) + b2 + 1
        elif b0 < 0xFC:
            sn = ((b0 & 0x1F) << 2) + 4
            dn, do = 0, 0
        else:
            sn, dn, do = b0 & 0x03, 0, 0
            last_command = True
        # Copy from source to dest.
        dest[dp : dp + sn] = source[sp : sp + sn]
        dp, sp = dp + sn, sp + sn
        # Copy from dest to dest (must be done byte by byte).
        for _ in range(dn):
            dest[dp] = dest[dp - do]
            dp += 1
    # Return the final result.
    return bytes(dest)
