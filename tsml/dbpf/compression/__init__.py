import zlib
from enum import Enum

from . import internal


class UnsupporedCompression(Exception):
    pass


class Compression(int, Enum):
    NONE = 0x0000
    ZLIB = 0x5A42
    DELETED = 0xFFE0
    STREAMABLE = 0xFFFE
    INTERNAL = 0xFFFF


def compress(data: bytes, compression: int) -> bytes:
    if compression == Compression.NONE:
        return data
    if compression == Compression.ZLIB:
        return zlib.compress(data)
    raise UnsupporedCompression(compression)


def decompress(data: bytes, compression: int) -> bytes:
    if compression == Compression.NONE:
        return data
    if compression == Compression.ZLIB:
        return zlib.decompress(data)
    if compression == Compression.INTERNAL:
        return internal.decompress(data)
    raise UnsupporedCompression(compression)
