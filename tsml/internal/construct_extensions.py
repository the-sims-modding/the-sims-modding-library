from construct import (
    Adapter,
    BytesInteger,
    Container,
    RangeError,
    SizeofError,
    Struct,
    Subconstruct,
)

Int128ul = BytesInteger(16, swapped=True)


class BoolAdapter(Adapter):
    def __init__(self, subcon, true=1, false=0):
        super().__init__(subcon)
        self._decode = lambda obj, ctx, path: bool(obj)
        self._encode = lambda obj, ctx, path: true if obj else false


class EnumAdapter(Adapter):
    def __init__(self, subcon, cls):
        super().__init__(subcon)
        self._decode = lambda obj, ctx, path: cls(obj)
        self._encode = lambda obj, ctx, path: obj.value


class NamedTupleAdapter(Adapter):
    def __init__(self, subcon, cls):
        super().__init__(subcon)
        self._decode = lambda obj, ctx, path: cls(*obj)
        self._encode = lambda obj, ctx, path: obj


class Dictionary(Subconstruct):
    def __init__(self, count, keysubcon, valuesubcon, discard=False):
        super().__init__(Struct("key" / keysubcon, "value" / valuesubcon))
        self.count = count
        self.discard = discard

    def _parse(self, stream, context, path):
        count = self.count
        if callable(count):
            count = count(context)
        if not 0 <= count:
            raise RangeError("invalid count %s" % (count,), path=path)
        obj = Container()
        for i in range(count):
            context._index = i
            e = self.subcon._parsereport(stream, context, path)
            if not self.discard:
                obj[e.key] = e.value
        return obj

    def _build(self, obj, stream, context, path):
        count = self.count
        if callable(count):
            count = count(context)
        if not 0 <= count:
            raise RangeError("invalid count %s" % (count,), path=path)
        if not len(obj) == count:
            raise RangeError(
                "expected %d elements, found %d" % (count, len(obj)), path=path
            )
        retdict = Container()
        for i, (k, v) in enumerate(obj.items()):
            context._index = i
            buildret = self.subcon._build(dict(key=k, value=v), stream, context, path)
            retdict[k] = buildret
        return retdict

    def _sizeof(self, context, path):
        try:
            count = self.count
            if callable(count):
                count = count(context)
        except (KeyError, AttributeError):
            raise SizeofError(
                "cannot calculate size, key not found in context", path=path
            )
        return count * self.subcon._sizeof(context, path)
