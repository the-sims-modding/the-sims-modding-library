from itertools import count


def bits_to_enum(bits, enum):
    values = set()
    for bit in count():
        bit_value = 1 << bit
        if not bits:
            break
        if not bit_value & bits:
            continue
        bits -= bit_value
        try:
            values.add(enum(bit))
        except ValueError:
            values.add(bit)
    return values


def enum_to_bits(values):
    bits = 0
    for value in values:
        if isinstance(value, int):
            bits |= 1 << value
        else:
            bits |= 1 << value.value
    return bits
