from typing import Union

# FNV-1 sizes that are xor folds of other sizes.
_fnv1_xor_folds = {
    24: 32,
}

# FNV-1 parameters for supported sizes.
_fnv1_parameters = {
    32: (2166136261, 16777619),
    64: (14695981039346656037, 1099511628211),
}


def fnv1(size: int, data: Union[str, bytes], *, set_high_bit: bool = False) -> int:
    # Convert strings to appropriate bytes.
    if isinstance(data, str):
        data = data.lower().encode()
    # Identify the hashing size.
    hash_size = _fnv1_xor_folds.get(size, size)
    # Get the FNV parameters.
    if hash_size not in _fnv1_parameters:
        raise ValueError(f"unsupported FNV-1 size: {size}")
    offset_basis, prime = _fnv1_parameters[hash_size]
    # Mask to remove overflow.
    hash_mask = (1 << hash_size) - 1
    # Main hashing algorithm.
    hash_ = offset_basis
    for byte in data:
        hash_ = hash_ * prime
        hash_ = hash_ ^ byte
        hash_ = hash_ & hash_mask
    # Xor-fold the result if required.
    if size < hash_size:
        fold_mask = (1 << size) - 1
        hash_ = (hash_ >> size) ^ (hash_ & fold_mask)
    # Set the highest bit if required.
    if set_high_bit:
        high_bit = 1 << (size - 1)
        hash_ = hash_ | high_bit
    # Return the final result.
    return hash_
