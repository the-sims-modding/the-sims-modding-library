from enum import IntEnum
from typing import NamedTuple

from construct import Adapter

from .hash import fnv1
from .locale import Locale, set_locale


class ResourceKey(NamedTuple):
    type: int
    group: int
    instance: int


class TGEI(Adapter):
    def _decode(self, obj, context, path):
        return ResourceKey(obj[0], obj[1], (obj[2] << 32) + obj[3])

    def _encode(self, obj, context, path):
        return obj.type, obj.group, obj.instance >> 32, obj.instance & 0xFFFFFFFF


class ResourceType(IntEnum):
    STBL = fnv1(32, "STBL")
    STRING_TABLE = STBL
    CAS_PART = 0x034AEECB


class ResourceGroup(IntEnum):
    NONE = 0x00000000
    MOD = 0x80000000


def generate_resource_key(
    name: str, resource_type: ResourceType, *, mod: bool = True, locale: Locale = None
) -> ResourceKey:
    instance = fnv1(64, name, set_high_bit=mod)
    if resource_type == ResourceType.STRING_TABLE:
        if locale is None:
            raise ValueError("must specify locale for string table resource")
        instance = set_locale(instance, locale)
    return ResourceKey(
        resource_type, ResourceGroup.MOD if mod else ResourceGroup.NONE, instance,
    )
