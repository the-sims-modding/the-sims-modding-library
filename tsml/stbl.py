from typing import Dict, SupportsBytes, Union

from construct import (
    Byte,
    Bytes,
    Const,
    FocusedSeq,
    Int16ul,
    Int32ul,
    Int64ul,
    Padding,
    Rebuild,
    len_,
    this,
)

from .internal.construct_extensions import Dictionary

_schema = FocusedSeq(
    "entries",
    Const(b"STBL"),
    Const(5, Int16ul),
    Const(0, Byte),
    "count" / Rebuild(Int64ul, len_(this.entries)),
    Padding(2),
    "memory_size"
    / Rebuild(Int32ul, lambda this: sum(len(v) + 1 for k, v in this.entries.items())),
    "entries"
    / Dictionary(
        this.count,
        Int32ul,
        FocusedSeq(
            "bytes",
            Const(0, Byte),
            "count" / Rebuild(Int16ul, len_(this.bytes)),
            "bytes" / Bytes(this.count),
        ),
    ),
)


class StringTable(Dict[int, str]):
    def __init__(self, init: Union[bytes, dict, SupportsBytes] = None) -> None:
        if not init:
            super().__init__()
            return
        if isinstance(init, dict):
            super().__init__(init)
            return
        data = _schema.parse(bytes(init))
        super().__init__((k, v.decode()) for k, v in data.items())

    def __bytes__(self):
        return _schema.build({k: v.encode() for k, v in self.items()})
