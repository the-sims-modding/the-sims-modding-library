from enum import Enum
from typing import Set, SupportsBytes, Union

from construct import (
    BitsInteger,
    BitStruct,
    Byte,
    Bytes,
    Check,
    Const,
    Default,
    Flag,
    Float32l,
    If,
    IfThenElse,
    Int16ul,
    Int32ul,
    Int64ul,
    Padding,
    PascalString,
    PrefixedArray,
    Struct,
    VarInt,
    this,
)

from .internal.construct_extensions import Int128ul
from .internal.util import bits_to_enum, enum_to_bits

_schema = Struct(
    "version" / Default(Int32ul, 46),
    Check(this.version != 45),  # Unknown version
    Check(26 <= this.version <= 46),
    "data_size" / Int32ul,
    "data"
    / Struct(
        Const(0, Int32ul),  # Preset count, not used in TS4
        "name" / PascalString(VarInt, "utf_16_be"),
        "display_index" / Float32l,
        "secondary_display_index" / Int16ul,
        "prototype_id" / Int32ul,
        "aural_material_hash" / Int32ul,
        "options"
        / BitStruct(
            "restrict_opposite_gender" / Flag,
            "random_in_live_mode" / Flag,
            "show_in_cas_demo" / Flag,
            "show_in_sim_info_panel" / Flag,
            "show_in_cas" / Flag,
            "random_in_cas" / Flag,
            "thumbnail_default" / Flag,
            "unused" / Flag,
        ),
        "options_extended"
        / If(
            this._.version >= 39,
            BitStruct(
                "unused" / BitsInteger(3),
                "unlockable_in_cas" / Flag,
                "unknown" / Flag,
                "female_body_type_defualt" / Flag,
                "male_body_type_default" / Flag,
                "restrict_opposite_frame" / Flag,
            ),
        ),
        "excluded_parts" / IfThenElse(this._.version >= 41, Int128ul, Int64ul),
        "excluded_modifier_regions"
        / IfThenElse(this._.version >= 36, Int64ul, Int32ul),
        "tags"
        / PrefixedArray(
            Int32ul,
            Struct(
                "category" / Int16ul,
                "tag" / IfThenElse(this._._._.version >= 37, Int32ul, Int16ul),
            ),
        ),
        "depricated" / Int32ul,
        "title_key" / Int32ul,
        "description_key" / Int32ul,
        "unknown_43" / If(this._.version >= 43, Int32ul),  # New in version 43
        "unique_texture_space" / Byte,
        "body_type" / Int32ul,
        "body_subtype" / Int32ul,
        "age_gender_flags" / Int32ul,
        "reserved" / If(this._.version >= 32, Int32ul),
        "pack_data"
        / If(
            this._.version >= 34,
            Struct("pack_id" / Int16ul, "pack_options" / Byte, Padding(9)),
        ),
        "unused"
        / If(this._.version < 34, Struct("unused" / Byte, If(this.unused, Byte))),
        "swatch_colors" / PrefixedArray(Byte, Int32ul),
        "buff_resource_key" / Byte,
        "variant_thumbnail_key" / Byte,
        "voice_effect_hash" / If(this._.version >= 28, Int64ul),
        "used_materials" / If(this._.version >= 30, PrefixedArray(Byte, Int32ul)),
        "hide_for_occults" / If(this._.version >= 31, Int32ul),
        "opposite_gender_part" / If(this._.version >= 38, Int64ul),
        "fallback_part" / If(this._.version >= 39, Int64ul),
        "unknown_46_1" / If(this._.version >= 46, Bytes(8)),  # New in version 45 and/or 46
        "unknown_44" / If(this._.version >= 44, Bytes(44)),  # New in version 44
        "unknown_46_2" / If(this._.version >= 46, PrefixedArray(Byte, Byte)),  # New in version 45 and/or 46
        "naked_key" / Byte,
        "parent_key" / Byte,
        "sort_layer" / Int32ul,
        "lods"
        / PrefixedArray(
            Byte,
            Struct(
                "level" / Byte,
                "unused" / Int32ul,
                "assets"
                / PrefixedArray(
                    Byte,
                    Struct(
                        "sorting" / Int32ul,
                        "specular_level" / Int32ul,
                        "cast_shadow" / Int32ul,
                    ),
                ),
                "keys" / PrefixedArray(Byte, Byte),
            ),
        ),
        "slot_keys" / PrefixedArray(Byte, Byte),
        "diffuse_key" / Byte,
        "shadow_key" / Byte,
        "composition_method" / Byte,
        "region_map_key" / Byte,
        "region_layer_overrides"
        / PrefixedArray(Byte, Struct("region" / Byte, "layer" / Float32l)),
        "normal_map_key" / Byte,
        "specular_map_key" / Byte,
        "normal_uv_body_type" / If(this._.version >= 27, Int32ul),
        "emission_map_key" / If(this._.version >= 29, Byte),
        "base_position_deformer_key" / If(this._.version >= 42, Byte),
    ),
    "resource_keys"
    / PrefixedArray(
        Byte, Struct("instance" / Int64ul, "group" / Int32ul, "type" / Int32ul)
    ),
)


class Occult(Enum):
    HUMAN = 0
    ALIEN = 1
    VAMPIRE = 2
    MERMAID = 3
    SPELLCASTER = 4


class CasPart:
    def __init__(self, init: Union[bytes, SupportsBytes]) -> None:
        self._raw = _schema.parse(bytes(init))
        self.hide_for_occults: Set[Union[Occult, int]] = bits_to_enum(
            self._raw.data.hide_for_occults, Occult
        )

    @property
    def name(self) -> str:
        return self._raw.data.name

    @property
    def show_in_cas(self) -> bool:
        return self._raw.data.options.show_in_cas

    @show_in_cas.setter
    def show_in_cas(self, value: bool) -> None:
        self._raw.data.options.show_in_cas = value

    @property
    def unlockable_in_cas(self) -> bool:
        return self._raw.data.options_extended.unlockable_in_cas

    @unlockable_in_cas.setter
    def unlockable_in_cas(self, value: bool) -> None:
        self._raw.data.options_extended.unlockable_in_cas = value

    def __bytes__(self):
        self._raw.data.hide_for_occults = enum_to_bits(self.hide_for_occults)
        return _schema.build(self._raw)
